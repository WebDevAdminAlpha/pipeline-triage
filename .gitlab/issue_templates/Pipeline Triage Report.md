<!--
The issue title should include the phrase "Pipeline Triage Report" and the week's start and end date, e.g.:

  Pipeline Triage Report 2020-11-04 to 2020-11-10

The issue description should outline the state of the pipeline/environments and briefly mention any highlights, e.g., application bugs found, noteworthy infrastructure issues, or changes that affected multiple tests, etc.
-->

# DRI

Please review the [responsiblities](https://about.gitlab.com/handbook/engineering/quality/guidelines/#responsibility) and [guidelines](https://about.gitlab.com/handbook/engineering/quality/guidelines/debugging-qa-test-failures/) if you have not done so recently.

Above all else, please remember that the aim of pipeline triage is to identify problems and try to get them **resolved** (ideally) _before_ they impact users.

You can view the [Testcases project list of issues for non-quarantined test results](https://gitlab.com/gitlab-org/quality/testcases/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=status%3A%3Aautomated&not[label_name][]=quarantine) to help identify failures and see if any were missed when reviewing pipelines.

<!--
Please @mention the Primary and Secondary DRI details  as mentioned in
https://about.gitlab.com/handbook/engineering/quality/guidelines/#schedule
-->
|           | EMEA | AMER | APAC |
|-----------|------|------|------|
| Primary   |      |      |      |
| Secondary |      |      |      |

<!--
Issues opened/reopened during last week that requires further triage or monitoring. Please copy over the issue handover section from last triage report.
-->
# Issues carried over from last week

# [Current known issues for CustomersDot project](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Quality%3Ae2e-test&label_name[]=staging%3A%3Afailed)

# Summary

## Highlights

## Staging :white_check_mark: / :x:

## Staging Orchestrated :white_check_mark: / :x:

## Pre-Production :white_check_mark: / :x:

## Production :white_check_mark: / :x:

## Nightly :white_check_mark: / :x:

## Master :white_check_mark: / :x:

<!--
Hand over issues opened this week but didn't get picked up or requires further action
-->
## Remaining Issues

<!--
The discussions should be organized to help keep track of the state of the tests.

For example, you can start one discussion each day in which you list the tests that fail, and edit the same comment to update the list.

Here is a table template you can use:

| Test | Issue | Pipeline | Failure / Notes |
|------|-------|----------|-----------------|
-->

## Handoff template
<details>
  <summary markdown="span">This is a template you can copy into the comments each day to help facilitate the hand-off between DRIs in different time zones.</summary>
  
| Test | Issue | Pipeline | Failure / Notes | Action Taken |
|------|-------|----------|-----------------|--------------|
|  |  |  |  |  |

#### Merged
* link to MR with note about the effects

#### In Progress
* link to MR
</details>

/label ~Quality
<!-- Please assign to all the Primary and Secondary DRIs -->
/assign
